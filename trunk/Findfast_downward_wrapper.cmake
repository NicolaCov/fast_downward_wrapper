#edit the following line to add the librarie's header files
FIND_PATH(fast_downward_wrapper_INCLUDE_DIR fast_downward_wrapper.h /usr/include/iridrivers /usr/local/include/iridrivers)

FIND_LIBRARY(fast_downward_wrapper_LIBRARY
    NAMES fast_downward_wrapper
    PATHS /usr/lib /usr/local/lib /usr/local/lib/iridrivers) 

IF (fast_downward_wrapper_INCLUDE_DIR AND fast_downward_wrapper_LIBRARY)
   SET(fast_downward_wrapper_FOUND TRUE)
ENDIF (fast_downward_wrapper_INCLUDE_DIR AND fast_downward_wrapper_LIBRARY)

IF (fast_downward_wrapper_FOUND)
   IF (NOT fast_downward_wrapper_FIND_QUIETLY)
      MESSAGE(STATUS "Found fast_downward_wrapper: ${fast_downward_wrapper_LIBRARY}")
   ENDIF (NOT fast_downward_wrapper_FIND_QUIETLY)
ELSE (fast_downward_wrapper_FOUND)
   IF (fast_downward_wrapper_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find fast_downward_wrapper")
   ENDIF (fast_downward_wrapper_FIND_REQUIRED)
ENDIF (fast_downward_wrapper_FOUND)

