begin_version
3
end_version
begin_metric
0
end_metric
11
begin_variable
var0
-1
2
Atom block_dir(o0, o1, dir3)
NegatedAtom block_dir(o0, o1, dir3)
end_variable
begin_variable
var1
-1
2
Atom block_dir(o0, o1, dir4)
NegatedAtom block_dir(o0, o1, dir4)
end_variable
begin_variable
var2
-1
2
Atom block_dir(o1, o0, dir3)
NegatedAtom block_dir(o1, o0, dir3)
end_variable
begin_variable
var3
-1
2
Atom block_dir(o1, o0, dir4)
NegatedAtom block_dir(o1, o0, dir4)
end_variable
begin_variable
var4
-1
2
Atom block_grasp(o0, o1)
NegatedAtom block_grasp(o0, o1)
end_variable
begin_variable
var5
-1
2
Atom block_grasp(o1, o0)
NegatedAtom block_grasp(o1, o0)
end_variable
begin_variable
var6
1
2
Atom new-axiom@1()
NegatedAtom new-axiom@1()
end_variable
begin_variable
var7
1
2
Atom new-axiom@3()
NegatedAtom new-axiom@3()
end_variable
begin_variable
var8
-1
2
Atom removed(o0)
NegatedAtom removed(o0)
end_variable
begin_variable
var9
-1
2
Atom removed(o1)
NegatedAtom removed(o1)
end_variable
begin_variable
var10
-1
2
Atom removed(o2)
NegatedAtom removed(o2)
end_variable
0
begin_state
0
0
0
0
0
0
0
0
1
1
1
end_state
begin_goal
3
8 0
9 0
10 0
end_goal
4
begin_operator
grasp-o0 
1
6 1
4
0 0 -1 1
0 1 -1 1
0 4 -1 1
0 8 -1 0
1
end_operator
begin_operator
grasp-o1 
1
7 1
4
0 2 -1 1
0 3 -1 1
0 5 -1 1
0 9 -1 0
1
end_operator
begin_operator
grasp-o2 
0
1
0 10 -1 0
1
end_operator
begin_operator
push-o1-dir1 
0
6
0 0 -1 1
0 1 -1 1
0 2 -1 1
0 3 -1 1
0 4 -1 1
0 5 -1 1
1
end_operator
2
begin_rule
1
4 1
7 0 1
end_rule
begin_rule
1
5 1
6 0 1
end_rule
