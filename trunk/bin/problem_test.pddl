(define (problem problem_test)
(:domain domain_test)
(:objects 
o1 - obj 
o2 - obj 
o3 - obj 
)
(:init 
(blockW o2 o1 )
(blockE o2 o1 )
(blockE o2 o3 )
(blockW o2 o3 )
(blockN o1 o2 )
(blockS o1 o2 )
(blockN o1 o3 )
(blockS o1 o3 )
(blockE o3 o2 )
(blockW o3 o2 )
(blockE o3 o1 )
(blockW o3 o1 )
)
(:goal 
(and 
 ;; we want the object o2 to be free 
 (not (exists (?x - obj)(blockN ?x o2)))
 (not (exists (?x - obj)(blockS ?x o2))) 
 (not (exists (?x - obj)(blockE ?x o2)))
 (not (exists (?x - obj)(blockW ?x o2)))
)
)
)