(define (problem problem_grasp_test)
(:domain domain_grasp_test)
(:objects 
o0 - obj 
o1 - obj 
o2 - obj 
)
(:init 
(= (total-cost) 1 )
(block_dir3 o1 o0 )
(block_dir3 o2 o0 )
(block_dir1 o2 o1 )
(block_dir2 o2 o1 )
(block_dir3 o2 o1 )
(block_dir4 o2 o1 )
(block_dir2 o0 o1 )
(block_dir1 o0 o2 )
(block_dir1 o1 o2 )
(block_dir2 o1 o2 )
(block_dir3 o1 o2 )
(block_dir4 o1 o2 )
(on o1 o2 )
(block_grasp o1 o2 )
(pushable o0 )
(pushable o1 )
(pushable o2 )
(graspable o0 )
(graspable o1 )
(graspable o2 )
)
(:goal 
(and 
				(grasped o0)
				(grasped o1)
				(grasped o2)
			)
)
(:metric minimize (total-cost))
)