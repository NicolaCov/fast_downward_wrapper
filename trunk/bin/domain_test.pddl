(define (domain domain_test)

	;; In this domain we are going to use the block predicates, to say that an object
	;; is blocked by another one. So it cannot be pushed until there is that object in 
	;; in the table. moreover, this take sinto account the direction of blocking.

	;; So, an object can be pushed only if it has no objects that block it. An object that
	;; is blocked on one direction but can be pushed to another one is said not be blocked
	;; by anything. 

	;; in this example we know try simply to seprate an undefined number of object
	;; (which will be defined in the problem.pddl). We have therefore to use
	;; "forall" and "when"
	(:requirements :adl :existential-preconditions :universal-preconditions :action-costs)
	(:types obj)
	(:predicates  
				
		; in this framework we are considerig that the object can be pushed in 4 direction
		; - 2 along the principal axis 
		; - 2 along the axis orthogonal to the principal one
		; An object B can be block another one in two ways:
		; -1) because the object A that will be moved will collide with B, so it block 
		;     the movement of A along the considered direction
		; -2) because the end effector of the robot will collide with it 
	    ;     the movement of A along the considered direction
	    (blockN ?o1 ?o2 - obj) ;; true if o1 blocks o2 in the north direction
		(blockS ?o1 ?o2 - obj) ;; true if o1 blocks o2 in the south direction
		(blockE ?o1 ?o2 - obj) ;; true if o1 blocks o2 in the est direction
		(blockW ?o1 ?o2 - obj) ;; true if o1 blocks o2 in the west direction
	)

	(:functions
		(total-cost) - number
	)

	;; push the object only in case it is adjacent to another one
	(:action push
		:parameters (?o - obj)
		:precondition 	(and	
				 			
				 			;; if there no exist objects that block ?o in all the directions
				 			(not (and
				 			(exists (?x - obj)(blockN ?x ?o))
				 			(exists (?x - obj)(blockS ?x ?o))
				 			(exists (?x - obj)(blockE ?x ?o))
				 			(exists (?x - obj)(blockW ?x ?o))
				 			))
						)
			     
		;; effects: free "o" and take off all the block predicates that have 
		;; something to do with it. Also free all the object that have no more blocking objects  
		:effect (and
			    	
				    (forall (?x - obj)
				    	(and

				    	;;; eliminate the blocking predicate to all the objects that are adjacent to "o"	
				    	(when (blockN ?o ?x) (not (blockN ?o ?x)))
					    (when (blockS ?o ?x) (not (blockS ?o ?x)))
					    (when (blockE ?o ?x) (not (blockE ?o ?x)))
					    (when (blockW ?o ?x) (not (blockW ?o ?x)))

					    ; eliminate all the others blocking conditions - the others objects blocking condition on ?o
					    (when (blockN ?x ?o) (not (blockN ?x ?o)))
					    (when (blockS ?x ?o) (not (blockS ?x ?o)))
					    (when (blockE ?x ?o) (not (blockE ?x ?o)))
					    (when (blockW ?x ?o) (not (blockW ?x ?o)))
					    )
				    )
				    (increase (total-cost) 1)
				    
			    )	
	)
)
