(define (problem problem_grasp)
(:domain domain_grasp)
(:objects 
o0 - obj 
o1 - obj 
o2 - obj 
dir1 - direction 
dir2 - direction 
dir3 - direction 
dir4 - direction 
)
(:init 
(block_dir o1 o0 dir3 )
(block_dir o0 o1 dir3 )
(block_dir o0 o1 dir4 )
(block_dir o1 o0 dir4 )
(block_grasp o1 o0 )
(block_grasp o0 o1 )
)
(:goal 
(and 
				(removed o0)
				(removed o1)
				(removed o2)
			)
)
)