begin_version
3
end_version
begin_metric
0
end_metric
26
begin_variable
var0
-1
2
Atom blocke(o2, o1)
NegatedAtom blocke(o2, o1)
end_variable
begin_variable
var1
-1
2
Atom blocke(o2, o3)
NegatedAtom blocke(o2, o3)
end_variable
begin_variable
var2
-1
2
Atom blocke(o3, o1)
NegatedAtom blocke(o3, o1)
end_variable
begin_variable
var3
-1
2
Atom blocke(o3, o2)
NegatedAtom blocke(o3, o2)
end_variable
begin_variable
var4
-1
2
Atom blockn(o1, o2)
NegatedAtom blockn(o1, o2)
end_variable
begin_variable
var5
-1
2
Atom blockn(o1, o3)
NegatedAtom blockn(o1, o3)
end_variable
begin_variable
var6
-1
2
Atom blocks(o1, o2)
NegatedAtom blocks(o1, o2)
end_variable
begin_variable
var7
-1
2
Atom blocks(o1, o3)
NegatedAtom blocks(o1, o3)
end_variable
begin_variable
var8
-1
2
Atom blockw(o2, o1)
NegatedAtom blockw(o2, o1)
end_variable
begin_variable
var9
-1
2
Atom blockw(o2, o3)
NegatedAtom blockw(o2, o3)
end_variable
begin_variable
var10
-1
2
Atom blockw(o3, o1)
NegatedAtom blockw(o3, o1)
end_variable
begin_variable
var11
-1
2
Atom blockw(o3, o2)
NegatedAtom blockw(o3, o2)
end_variable
begin_variable
var12
1
2
Atom new-axiom@0(o2)
NegatedAtom new-axiom@0(o2)
end_variable
begin_variable
var13
1
2
Atom new-axiom@0(o3)
NegatedAtom new-axiom@0(o3)
end_variable
begin_variable
var14
1
2
Atom new-axiom@1(o2)
NegatedAtom new-axiom@1(o2)
end_variable
begin_variable
var15
1
2
Atom new-axiom@1(o3)
NegatedAtom new-axiom@1(o3)
end_variable
begin_variable
var16
1
2
Atom new-axiom@2(o1)
NegatedAtom new-axiom@2(o1)
end_variable
begin_variable
var17
1
2
Atom new-axiom@2(o2)
NegatedAtom new-axiom@2(o2)
end_variable
begin_variable
var18
1
2
Atom new-axiom@2(o3)
NegatedAtom new-axiom@2(o3)
end_variable
begin_variable
var19
1
2
Atom new-axiom@3(o1)
NegatedAtom new-axiom@3(o1)
end_variable
begin_variable
var20
1
2
Atom new-axiom@3(o2)
NegatedAtom new-axiom@3(o2)
end_variable
begin_variable
var21
1
2
Atom new-axiom@3(o3)
NegatedAtom new-axiom@3(o3)
end_variable
begin_variable
var22
1
2
Atom new-axiom@4()
NegatedAtom new-axiom@4()
end_variable
begin_variable
var23
1
2
Atom new-axiom@5()
NegatedAtom new-axiom@5()
end_variable
begin_variable
var24
1
2
Atom new-axiom@6()
NegatedAtom new-axiom@6()
end_variable
begin_variable
var25
1
2
Atom new-axiom@7()
NegatedAtom new-axiom@7()
end_variable
0
begin_state
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
0
end_state
begin_goal
4
22 1
23 1
24 1
25 1
end_goal
12
begin_operator
push o1
0
8
0 0 -1 1
0 2 -1 1
0 4 -1 1
0 5 -1 1
0 6 -1 1
0 7 -1 1
0 8 -1 1
0 10 -1 1
1
end_operator
begin_operator
push o1
0
8
0 0 -1 1
0 2 -1 1
0 4 -1 1
0 5 -1 1
0 6 -1 1
0 7 -1 1
0 8 -1 1
0 10 -1 1
1
end_operator
begin_operator
push o1
1
16 1
8
0 0 -1 1
0 2 -1 1
0 4 -1 1
0 5 -1 1
0 6 -1 1
0 7 -1 1
0 8 -1 1
0 10 -1 1
1
end_operator
begin_operator
push o1
1
19 1
8
0 0 -1 1
0 2 -1 1
0 4 -1 1
0 5 -1 1
0 6 -1 1
0 7 -1 1
0 8 -1 1
0 10 -1 1
1
end_operator
begin_operator
push o2
1
12 1
8
0 0 -1 1
0 1 -1 1
0 3 -1 1
0 4 -1 1
0 6 -1 1
0 8 -1 1
0 9 -1 1
0 11 -1 1
1
end_operator
begin_operator
push o2
1
14 1
8
0 0 -1 1
0 1 -1 1
0 3 -1 1
0 4 -1 1
0 6 -1 1
0 8 -1 1
0 9 -1 1
0 11 -1 1
1
end_operator
begin_operator
push o2
1
17 1
8
0 0 -1 1
0 1 -1 1
0 3 -1 1
0 4 -1 1
0 6 -1 1
0 8 -1 1
0 9 -1 1
0 11 -1 1
1
end_operator
begin_operator
push o2
1
20 1
8
0 0 -1 1
0 1 -1 1
0 3 -1 1
0 4 -1 1
0 6 -1 1
0 8 -1 1
0 9 -1 1
0 11 -1 1
1
end_operator
begin_operator
push o3
1
13 1
8
0 1 -1 1
0 2 -1 1
0 3 -1 1
0 5 -1 1
0 7 -1 1
0 9 -1 1
0 10 -1 1
0 11 -1 1
1
end_operator
begin_operator
push o3
1
15 1
8
0 1 -1 1
0 2 -1 1
0 3 -1 1
0 5 -1 1
0 7 -1 1
0 9 -1 1
0 10 -1 1
0 11 -1 1
1
end_operator
begin_operator
push o3
1
18 1
8
0 1 -1 1
0 2 -1 1
0 3 -1 1
0 5 -1 1
0 7 -1 1
0 9 -1 1
0 10 -1 1
0 11 -1 1
1
end_operator
begin_operator
push o3
1
21 1
8
0 1 -1 1
0 2 -1 1
0 3 -1 1
0 5 -1 1
0 7 -1 1
0 9 -1 1
0 10 -1 1
0 11 -1 1
1
end_operator
14
begin_rule
2
0 1
2 1
16 0 1
end_rule
begin_rule
1
1 1
18 0 1
end_rule
begin_rule
1
3 1
17 0 1
end_rule
begin_rule
1
3 1
24 0 1
end_rule
begin_rule
1
4 1
12 0 1
end_rule
begin_rule
1
4 1
22 0 1
end_rule
begin_rule
1
5 1
13 0 1
end_rule
begin_rule
1
6 1
14 0 1
end_rule
begin_rule
1
6 1
23 0 1
end_rule
begin_rule
1
7 1
15 0 1
end_rule
begin_rule
2
8 1
10 1
19 0 1
end_rule
begin_rule
1
9 1
21 0 1
end_rule
begin_rule
1
11 1
20 0 1
end_rule
begin_rule
1
11 1
25 0 1
end_rule
