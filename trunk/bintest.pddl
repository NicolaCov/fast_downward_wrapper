(define (problem test)
(:domain domain_test)
(:objects aaao1 - obj 
o2 - obj 
)
(:init 
(blockW o2 o1 )
(blockE o2 o1 )
(blockN o1 o2 )
(blockS o1 o2 )
)
(:goal 
(and 
 ;; we want the object o2 to be free 
 (not (exists (?x - obj)(blockN ?x o2)))
 (not (exists (?x - obj)(blockS ?x o2))) 
 (not (exists (?x - obj)(blockE ?x o2)))
 (not (exists (?x - obj)(blockW ?x o2)))
)
)
)