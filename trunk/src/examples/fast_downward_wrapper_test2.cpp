/**
@example fast_downward_wrapper_test2.cpp
 
@b Description:
This program test the CFast_Downward_Wrapper api for the scene in cluttered_6.pcd
The predicates that here are built by hands have been copied from the output of table_clearing_planning_test.cpp
for the scene cluttered_6.pcd.
 
@b Usage:
@code
$ ../bin/fast_downward_wrapper_test2
@endcode
 
This example tries to get the plan for the cluttered_6.pcd scene. Where object 0 is the one alone,
object 1 is the one in diagonal and laying on the other object, which is the object 2. Too see
this scene:
@verbatim
pcl_viewer cluttered_6.pcd
@endverbatim
*/
#include "fast_downward_wrapper.h"

int main(int argc, char *argv[])
{
	CFast_Downward_Wrapper fdw;
	fdw.setFastDownwardFolder("/home/ncovallero/FastDownward");
	fdw.setCurrentFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
	fdw.setProblemFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
	fdw.setDomainFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
	fdw.setDomainPDDLFile("domain_grasp_test");
	fdw.setProblemPDDLFile("problem_grasp_test");	

	Fast_Downward_Wrapper::Objects objects;
	Fast_Downward_Wrapper::Object obj;
	obj.object = "o0";
	obj.type = "obj";
	objects.push_back(obj);
	obj.object = "o1";
	objects.push_back(obj);
	obj.object = "o2";
	objects.push_back(obj);
	fdw.setObjects(objects);

	// create predicates for a simple problem
	Fast_Downward_Wrapper::SymbolicPredicates sym_predicates;
	Fast_Downward_Wrapper::SymbolicPredicate sp;
	sp.name = "block_dir3";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o0");
	sym_predicates.push_back(sp);

	sp.name = "block_dir3";
	sp.objects.resize(0);
	sp.objects.push_back("o2");
	sp.objects.push_back("o0");
	sym_predicates.push_back(sp);

	sp.name = "block_dir1";
	sp.objects.resize(0);
	sp.objects.push_back("o2");
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);

	sp.name = "block_dir2";
	sp.objects.resize(0);
	sp.objects.push_back("o2");
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);
	
	sp.name = "block_dir3";
	sp.objects.resize(0);
	sp.objects.push_back("o2");
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);

	sp.name = "block_dir4";
	sp.objects.resize(0);
	sp.objects.push_back("o2");
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);

	sp.name = "block_dir2";
	sp.objects.resize(0);
	sp.objects.push_back("o0");
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);

	sp.name = "block_dir1";
	sp.objects.resize(0);
	sp.objects.push_back("o0");
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);

	sp.name = "block_dir1";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);

	sp.name = "block_dir2";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);

	sp.name = "block_dir3";	
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);

	sp.name = "block_dir4";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);

	sp.name = "on";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);

	sp.name = "block_grasp";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);

	sp.name = "pushable";
	sp.objects.resize(0);
	sp.objects.push_back("o0");
	sym_predicates.push_back(sp);

	sp.name = "pushable";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);

	sp.name = "pushable";
	sp.objects.resize(0);
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);

	sp.name = "graspable";
	sp.objects.resize(0);
	sp.objects.push_back("o0");
	sym_predicates.push_back(sp);

	sp.name = "graspable";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);

	sp.name = "graspable";
	sp.objects.resize(0);
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);

	fdw.setSymbolicPredicates(sym_predicates);

	std::string goal;
	goal = "(and \n\
				(grasped o0)\n\
				(grasped o1)\n\
				(grasped o2)\n\
			)";


	fdw.setGoal(goal);

	fdw.addTotalCost(1);
	fdw.writeProblemPDDLFile();

	fdw.setHeuristic("h=ff()");
	fdw.setSearch("eager_greedy(h,preferred=h)");
	fdw.computePlan();
	fdw.printPlan();
	Fast_Downward_Wrapper::Plan plan = fdw.getPlan();
}
