/**
@example fast_downward_wrapper_test.cpp
 
@b Description:
This program test the CFast_Downward_Wrapper api.
 
@b Usage:
@code
$ ../bin/fast_downward_wrapper_test
@endcode
 
This example tries to get the plan of the following problem
@image html problem_test.png
defined in the domain "domain_test.pddl"
where the goal is to have the object 2 to be separated from the others one.
*/
#include "fast_downward_wrapper.h"

int main(int argc, char *argv[])
{
	CFast_Downward_Wrapper fdw;
	fdw.setFastDownwardFolder("/home/ncovallero/FastDownward");
	fdw.setCurrentFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
	fdw.setProblemFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
	fdw.setDomainFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
	fdw.setDomainPDDLFile("domain_test");
	fdw.setProblemPDDLFile("problem_test");

	Fast_Downward_Wrapper::Objects objects;
	Fast_Downward_Wrapper::Object obj;
	obj.object = "o1";
	obj.type = "obj";
	objects.push_back(obj);
	obj.object = "o2";
	objects.push_back(obj);
	obj.object = "o3";
	objects.push_back(obj);
	fdw.setObjects(objects);

	// create predicates for a simple problem
	Fast_Downward_Wrapper::SymbolicPredicates sym_predicates;
	Fast_Downward_Wrapper::SymbolicPredicate sp;
	sp.name = "blockW";
	sp.objects.resize(0);
	sp.objects.push_back("o2");
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);
	sp.name = "blockE";
	sp.objects.resize(0);
	sp.objects.push_back("o2");
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);
	sp.name = "blockE";
	sp.objects.resize(0);
	sp.objects.push_back("o2");
	sp.objects.push_back("o3");
	sym_predicates.push_back(sp);
	sp.name = "blockW";
	sp.objects.resize(0);
	sp.objects.push_back("o2");
	sp.objects.push_back("o3");
	sym_predicates.push_back(sp);

	sp.name = "blockN";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);
	sp.name = "blockS";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);
	sp.name = "blockN";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o3");
	sym_predicates.push_back(sp);
	sp.name = "blockS";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o3");
	sym_predicates.push_back(sp);

	sp.name = "blockE";
	sp.objects.resize(0);
	sp.objects.push_back("o3");
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);
	sp.name = "blockW";
	sp.objects.resize(0);
	sp.objects.push_back("o3");
	sp.objects.push_back("o2");
	sym_predicates.push_back(sp);
	sp.name = "blockE";
	sp.objects.resize(0);
	sp.objects.push_back("o3");
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);
	sp.name = "blockW";
	sp.objects.resize(0);
	sp.objects.push_back("o3");
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);

	fdw.setSymbolicPredicates(sym_predicates);

	std::string goal;
	goal = "(and \n ;; we want the object o2 to be free \n (not (exists (?x - obj)(blockN ?x o2)))\n (not (exists (?x - obj)(blockS ?x o2))) \n (not (exists (?x - obj)(blockE ?x o2)))\n (not (exists (?x - obj)(blockW ?x o2)))\n)";

	fdw.setGoal(goal);

	fdw.writeProblemPDDLFile();

	fdw.setHeuristic("h=ff()");
	fdw.setSearch("eager_greedy(h,preferred=h)");
	fdw.computePlan();
	fdw.printPlan();
	Fast_Downward_Wrapper::Plan plan = fdw.getPlan();
}
