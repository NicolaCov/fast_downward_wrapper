/**
@example fast_downward_wrapper_test2.cpp
 
@b Description:
This program test the CFast_Downward_Wrapper api for the scene in cluttered_6.pcd
The predicates that here are built by hands have been copied from the output of table_clearing_planning_test.cpp
for the scene cluttered_6.pcd.
 
@b Usage:
@code
$ ../bin/fast_downward_wrapper_test2
@endcode
 
This example tries to get the plan for the cluttered_6.pcd scene. Where object 0 is the one alone,
object 1 is the one in diagonal and laying on the other object, which is the object 2. Too see
this scene:
@verbatim
pcl_viewer cluttered_6.pcd
@endverbatim
*/
#include "fast_downward_wrapper.h"

int main(int argc, char *argv[])
{
	CFast_Downward_Wrapper fdw;
	fdw.setFastDownwardFolder("/home/ncovallero/FastDownward");
	fdw.setCurrentFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
	fdw.setProblemFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
	fdw.setDomainFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
	fdw.setDomainPDDLFile("domain_grasp");
	fdw.setProblemPDDLFile("problem_grasp");	

	Fast_Downward_Wrapper::Objects objects;
	Fast_Downward_Wrapper::Object obj;
	obj.object = "o0";
	obj.type = "obj";
	objects.push_back(obj);
	obj.object = "o1";
	objects.push_back(obj);
	obj.object = "o2";
	objects.push_back(obj);
	obj.object = "dir1";
	obj.type = "direction";
	objects.push_back(obj);
	obj.object = "dir2";
	objects.push_back(obj);
	obj.object = "dir3";
	objects.push_back(obj);
	obj.object = "dir4";
	objects.push_back(obj);
	fdw.setObjects(objects);

	// create predicates for a simple problem
	Fast_Downward_Wrapper::SymbolicPredicates sym_predicates;
	
	Fast_Downward_Wrapper::SymbolicPredicate sp;
	sp.name = "block_dir";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o0");
	sp.objects.push_back("dir3");
	sym_predicates.push_back(sp);

	sp.name = "block_dir";
	sp.objects.resize(0);
	sp.objects.push_back("o0");
	sp.objects.push_back("o1");
	sp.objects.push_back("dir3");
	sym_predicates.push_back(sp);

	sp.name = "block_dir";
	sp.objects.resize(0);
	sp.objects.push_back("o0");
	sp.objects.push_back("o1");
	sp.objects.push_back("dir4");
	sym_predicates.push_back(sp);

	sp.name = "block_dir";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o0");
	sp.objects.push_back("dir4");
	sym_predicates.push_back(sp);
	
	sp.name = "block_grasp";
	sp.objects.resize(0);
	sp.objects.push_back("o1");
	sp.objects.push_back("o0");
	sym_predicates.push_back(sp);

	sp.name = "block_grasp";
	sp.objects.resize(0);
	sp.objects.push_back("o0");
	sp.objects.push_back("o1");
	sym_predicates.push_back(sp);

	fdw.setSymbolicPredicates(sym_predicates);

	std::string goal;
	goal = "(and \n\
				(removed o0)\n\
				(removed o1)\n\
				(removed o2)\n\
			)";

	fdw.setGoal(goal);

	
	fdw.writeProblemPDDLFile();

	

	// write the domain
	std::vector<std::string> types;
	types.push_back("obj");
	types.push_back("direction");
	fdw.setTypes(types);
	fdw.addTotalCost();	

	// domain predicates
	Fast_Downward_Wrapper::DomainSymbolicPredicates domain_symbolic_predicates;
	domain_symbolic_predicates.resize(6);
	domain_symbolic_predicates[0].name = "block_dir";
	domain_symbolic_predicates[0].variables.resize(3);
	domain_symbolic_predicates[0].variables[0] = "o1";
	domain_symbolic_predicates[0].variables[1] = "o2";
	domain_symbolic_predicates[0].variables[2] = "d";
	domain_symbolic_predicates[0].types.resize(3);
	domain_symbolic_predicates[0].types[0] = "obj";
	domain_symbolic_predicates[0].types[1] = "obj";
	domain_symbolic_predicates[0].types[2] = "direction";
	
	domain_symbolic_predicates[1].name = "on";
	domain_symbolic_predicates[1].variables.resize(2);
	domain_symbolic_predicates[1].variables[0] = "o1";
	domain_symbolic_predicates[1].variables[1] = "o2";
	domain_symbolic_predicates[1].types.resize(2);
	domain_symbolic_predicates[1].types[0] = "obj";
	domain_symbolic_predicates[1].types[1] = "obj";
	
	domain_symbolic_predicates[2] = domain_symbolic_predicates[1];
	domain_symbolic_predicates[2].name = "block_grasp";

	domain_symbolic_predicates[3].name = "removed";
	domain_symbolic_predicates[3].variables.resize(1);
	domain_symbolic_predicates[3].variables[0] = "o";
	domain_symbolic_predicates[3].types.push_back("obj");

	domain_symbolic_predicates[4].name = "ik_unfeasible_dir";
	domain_symbolic_predicates[4].variables.resize(2);
	domain_symbolic_predicates[4].variables[0] = "o";
	domain_symbolic_predicates[4].variables[1] = "d";
	domain_symbolic_predicates[4].types.resize(2);
	domain_symbolic_predicates[4].types[0] = "obj";
	domain_symbolic_predicates[4].types[1] = "direction";

	domain_symbolic_predicates[5].name = "ik_unfeasible_grasp";
	domain_symbolic_predicates[5].variables.resize(1);
	domain_symbolic_predicates[5].variables[0] = "o";
	domain_symbolic_predicates[5].types.push_back("obj");

	fdw.setDomainPredicates(domain_symbolic_predicates);

	// actions
	Fast_Downward_Wrapper::DomainActions domain_actions;
	domain_actions.resize(4);

	domain_actions[0].action_name = fdw.getGraspActionName(0);
	domain_actions[0].preconditions = fdw.getActionGraspPrecondition("o0");
	domain_actions[0].effects = fdw.getActionGraspEffect("o0", 5);
	
	domain_actions[1].action_name = fdw.getGraspActionName(1);
	domain_actions[1].preconditions = fdw.getActionGraspPrecondition("o1");
	domain_actions[1].effects = fdw.getActionGraspEffect("o1", 5);
	
	domain_actions[2].action_name = fdw.getGraspActionName(2);
	domain_actions[2].preconditions = fdw.getActionGraspPrecondition("o2");			
	domain_actions[2].effects = fdw.getActionGraspEffect("o2", 20);

	domain_actions[3].action_name = fdw.getPushActionName(1,1);
	domain_actions[3].preconditions = fdw.getActionPushPrecondition("o1","dir1");			
	domain_actions[3].effects = fdw.getActionPushEffect("o1", "dir1",20);	


	fdw.setDomainActions(domain_actions);

	fdw.writeDomainPDDLFile();

	fdw.setHeuristic("h=ff()");
	fdw.setSearch("eager_greedy(h,preferred=h)");
	fdw.computePlan();
	fdw.printPlan();
	Fast_Downward_Wrapper::Plan plan = fdw.getPlan();
}
