#ifndef _FAST_DOWNWARD_WRAPPER_H
#define _FAST_DOWNWARD_WRAPPER_H

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <algorithm>


namespace Fast_Downward_Wrapper
{
	/**
	 * @brief Object struct
	 * @details Define the name of the object and their type, in case they have
	 * 
	 */
	struct Object
	{
		std::string object;
		std::string type;
	};
	typedef std::vector<Object> Objects;

	/**
	 * @brief Action struct
	 * @details Define the action, its name and the interest objects for such an action. 
	 * It is used in the plan.
	 * 
	 */
	struct Action{
		std::string action;
		std::vector<std::string> objects;
	};
	typedef std::vector<Action> Actions;

	/**
	 * @brief Plan
	 * @details The plan is define as a vector of actions, each one with a name and the interesting objects, 
	 * and the plan also has a cost.
	 * 
	 */
	struct Plan
	{
		Actions actions;
		unsigned int cost;
	};

	/**
	 * @brief Symbolic Predicate Struct
	 * @details It wraps the name of the predicate and all the interesting objects related to the 
	 *  predicate. 
	 * 
	 */
	struct SymbolicPredicate
	{
		std::string name;
		std::vector<std::string> objects;
	};
	typedef std::vector<SymbolicPredicate> SymbolicPredicates;

  struct DomainSymbolicPredicate

  {
      std::string name;
      std::vector<std::string> variables;
      std::vector<std::string> types; // one type per variable
  };
  typedef std::vector<DomainSymbolicPredicate> DomainSymbolicPredicates;

  struct DomainAction
  {
      std::string action_name;
      
      struct Parameters
      {
          std::vector<std::string> params;
          std::vector<std::string> types; // this has to be as many elements as "params", also empty are fine
      }parameters;

      std::string preconditions;
      
      std::string effects;
      
      uint cost; // cost of the action
  };
  typedef std::vector<DomainAction> DomainActions;

}

/**
 * @brief CFast_Downward_Wrapper class to handle automaticaly the Fast Downward Planner
 * @details This class lets the user to define a problem and get the plan using the Fast Downward Planner.
 * 			The domain.pddl has to be defined offline by the users, remember that the name of the file .pddl and the domain
 * 			have to be the same. The problem can be defined automatically through this class.
 * 			Until know it only lets to handle symbolic predicates. To fit your functionality could easily need other methods to be added.
 * 			\n
 * 			Basic usage of the class
 * 	\code
 * 	CFast_Downward_Wrapper fdw;
 *	fdw.setFastDownwardFolder("/home/ncovallero/FastDownward");
 *	fdw.setCurrentFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
 *	fdw.setProblemFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
 *	fdw.setDomainFolder("/home/ncovallero/iri-lab/labrobotica/algorithms/fast_downward_wrapper/trunk/bin");
 *	fdw.setDomainPDDLFile("domain_test");
 *	fdw.setProblemPDDLFile("test");
 *	Fast_Downward_Wrapper::Objects objects;
 *	// define here the objects ...
 *	Fast_Downward_Wrapper::SymbolicPredicates sym_predicates;
 *	// define here the symbolic predicates ...
 *	std::string goal;
 *	goal = "(and \n ;; we want the object o2 to be free \n (not (exists (?x - obj)(blockN ?x o2)))\n (not (exists (?x - obj)(blockS ?x o2))) \n (not (exists (?x - obj)(blockE ?x o2)))\n (not (exists (?x - obj)(blockW ?x o2)))\n)";
 *
 *	fdw.setGoal(goal);
 *
 *	fdw.writeProblemPDDLFile();
 *
 *	fdw.setHeuristic("h=ff()");
 *	fdw.setSearch("eager_greedy(h,preferred=h)");
 *	fdw.computePlan();
 *	Fast_Downward_Wrapper::Plan plan = fdw.getPlan();
 * 	\endcode
 */
class CFast_Downward_Wrapper
{

  std::string 	domain_file_name,
  				domain_folder,
  				problem_folder,
		 		problem_file_name,
		 		fast_downward_folder,
		 		current_folder;

  std::string goal;

  std::string heuristic;
  std::string search;
  
  Fast_Downward_Wrapper::SymbolicPredicates sym_predicates;
  Fast_Downward_Wrapper::Objects objects;
  Fast_Downward_Wrapper::Plan plan;

  std::ofstream problem_file, domain_file;


  // action costs parameters
  bool use_cost;
  uint init_cost;

  // domain pddl - all the required stuff to define nicely the domain
  // the names are decleared above
  std::vector<std::string> requirements, types;
  Fast_Downward_Wrapper::DomainSymbolicPredicates domain_symbolic_predicates;
  Fast_Downward_Wrapper::DomainActions domain_actions;

  
  // -------------- WRITING FUNCTIONS ------------------
  void startProblem();
  void writeObjects();
  void writePredicates();
  void writeGoal();
  void endProblem();

  void startDomain();
  void writeRequirements();
  void writeTypes();
  void writeDomainPredicates();
  void writeCost();
  void writeActions();
  void endDomain();
  // ---------------------------------------------------


  /**
   * @details Call the binary of the planner. Method called by CFast_Downward_Wrapper::computePlan(). 
   */
  void callPlanner();

  public:
    CFast_Downward_Wrapper();
    CFast_Downward_Wrapper(std::string folder);
    ~CFast_Downward_Wrapper();

    /**
     * @details Set the root folder where FastDownward planner is installed [FULL PATH].
     * 
     * @param folder 
     */
    void setFastDownwardFolder(std::string folder);

    /**
     * @details Set the folder where the domain file is located [FULL PATH].
     * 
     * @param domain_folder 
     */
    void setDomainFolder(std::string domain_folder);

    /**
     * @details Set the current folder where is located the binary of YOUR program [FULL PATH].
     * 
     * @param current_folder 
     */
    void setCurrentFolder(std::string current_folder);

    /**
     * @details Set the folder where the problem file is located [FULL PATH].
     * 
     * @param problem_folder 
     */
    void setProblemFolder(std::string problem_folder);

    /**
     * @details Set the name of the domain file [without the ".pddl" extension]. IMPORTANT
     * the domain_file_name has to be both the name of file and the name of the domain (domain domain_file_name)
     * 
     * @param domain_file_name 
     */
    void setDomainPDDLFile(std::string domain_file_name);

    /**
     * @details Set the name of the problem file [without the ".pddl" extension]
     * 
     * @param problem_file_name 
     */
    void setProblemPDDLFile(std::string problem_file_name);

    /**
     * @brief Set the objects of the problem
     * @details Set the objects of the problem
     * 
     * @param objects 
     */
    void setObjects(Fast_Downward_Wrapper::Objects objects);

    /**
     * @details Set symbolic predicates
     * 
     * @param sym_predicates 
     */
    void setSymbolicPredicates(Fast_Downward_Wrapper::SymbolicPredicates sym_predicates);

    /**
     * @brief Set the goal
     * @details Set the goal of the problem. Due to big variety of possible expression 
     * in this field, the goal expression has to be specified by the user as an unique string.
     * 
     * @param goal 
     */
    void setGoal(std::string goal);

    /**
     * @brief Set heuristic method
     * @details The heuristic method is jsut a string which is the same that will be put in the command
     * to call the planner. See the example.
     * 
     * @param heuristic 
     */
    void setHeuristic(std::string heuristic);

    /**
     * @brief Set search method
     * @details The search method is jsut a string which is the same that will be put in the command
     * to call the planner. See the example.
     * 
     * @param search
     */
    void setSearch(std::string search);

    /**
     * @brief Set the requirements
     * @details Set the requirements
     * 
     * @param requirements 
     */
    void setRequirements(std::vector<std::string> requirements);

    void setDomainPredicates(Fast_Downward_Wrapper::DomainSymbolicPredicates domain_symbolic_predicates);

    void setDomainActions(Fast_Downward_Wrapper::DomainActions domain_actions);

    /**
     * @brief Set the types
     * @details Set the types
     * 
     * @param types 
     */
    void setTypes(std::vector<std::string> types);

    void addTotalCost(uint init_cost = 0);

    /**
     * @brief Write the problem file .pddl 
     * @details Write the problem file .pddl with all the data specified. 
     * 
     * @param over_write False if you don't want to overwrite the problem.pddl file, 
     */
    void writeProblemPDDLFile(bool over_write = true);


    /**
     * @brief Write the domain file .pddl
     * @details [long description]
     * 
     * @param over_write [description]
     */
    void writeDomainPDDLFile(bool over_write = true);

    /**
     * @brief Compute the plan
     * @details Call the planner and save it.
     * 
     * @return True if there is a feasible plan
     */
    bool computePlan();

    /**
     * @brief Print in the terminal the obtained plan.
     */
    void printPlan();

    /**
     * @brief Get the plan
     * @details Get the plan
     * @return the plan
     */
    Fast_Downward_Wrapper::Plan getPlan();


    // HELP WRITING FUNCTIONS
    // The followings are functions specific for my project, not useful for a generic
    // wrapping of the fast downward planner.
    // The followings are functions to help to write the effects and precondictions
    // for the table_clearing library. In this case the effects and precond are the same, 
    // only one parameters changes. 
    std::string getActionGraspPrecondition(std::string obj);
    std::string getActionGraspEffect(std::string obj, uint cost);
    std::string getGraspActionName(std::string obj);
    std::string getGraspActionName(uint obj);

    std::string getActionPushPrecondition(std::string obj, std::string dir);
    std::string getActionPushEffect(std::string obj, std::string dir, uint cost);
    std::string getPushActionName(std::string obj, std::string dir);
    std::string getPushActionName(uint obj, uint dir);
};

#endif
