#include "fast_downward_wrapper.h"

CFast_Downward_Wrapper::CFast_Downward_Wrapper()
{
	this->use_cost = false;
	this->init_cost = 0;

	this->requirements.resize(4);
	this->requirements[0] ="adl";
	this->requirements[1] ="existential-preconditions";
	this->requirements[2] ="universal-preconditions";
	this->requirements[3] ="action-costs";
	

}
CFast_Downward_Wrapper::CFast_Downward_Wrapper(std::string folder)
{
	this->setFastDownwardFolder(folder);
	this->use_cost = false;
	this->init_cost = 0;

	this->requirements.resize(4);
	this->requirements[0] ="adl";
	this->requirements[1] ="existential-preconditions";
	this->requirements[2] ="universal-preconditions";
	this->requirements[3] ="action-costs";
}

CFast_Downward_Wrapper::~CFast_Downward_Wrapper()
{
}

void CFast_Downward_Wrapper::startProblem()
{
	this->problem_file << "(define ";
	this->problem_file << "(problem " << this->problem_file_name << ")\n";
	this->problem_file << "(:domain " << this->domain_file_name << ")\n";
}
void CFast_Downward_Wrapper::writeObjects()
{
	this->problem_file << "(:objects \n";
	for (unsigned int i = 0; i < this->objects.size(); ++i)
	{
		this->problem_file << this->objects[i].object;
		if(this->objects[i].type.length() != 0)
		{
			this->problem_file << " - " << this->objects[i].type;
		}
		this->problem_file << " \n";
	}
	this->problem_file << ")\n";	
}
void CFast_Downward_Wrapper::writePredicates()
{
	this->problem_file << "(:init \n";
	if(this->use_cost) // add cost
		this->problem_file  << 	"(= (total-cost) " << this->init_cost << " )\n";
	for (unsigned int i = 0; i < this->sym_predicates.size(); ++i)
	{
		this->problem_file  << "(" << this->sym_predicates[i].name << " ";
		for (unsigned int h = 0; h < this->sym_predicates[i].objects.size() ; ++h)
		{
			this->problem_file << this->sym_predicates[i].objects[h] << " ";
		}
		this->problem_file << ")\n";
	}
	this->problem_file << ")\n";
}
void CFast_Downward_Wrapper::writeGoal()
{
	this->problem_file << "(:goal \n";
	this->problem_file << this->goal << "\n";
	this->problem_file << ")\n";

}
void CFast_Downward_Wrapper::endProblem()
{
	this->problem_file << ")";
}
void CFast_Downward_Wrapper::startDomain()
{
	this->domain_file << "(define ";
	this->domain_file << "(domain " << this->domain_file_name << ")\n";
}
void CFast_Downward_Wrapper::writeRequirements()
{
	this->domain_file << "(:requirements ";
	for (uint i = 0; i < this->requirements.size(); ++i)
	{
		this->domain_file << ":" << this->requirements[i] << " ";
	}
	this->domain_file << ")\n";	
}
void CFast_Downward_Wrapper::writeTypes()
{
	this->domain_file << "(:types ";
	for (uint i = 0; i < this->types.size(); ++i)
	{
		this->domain_file << this->types[i] << " ";
	}
	this->domain_file << ")\n";		
}
void CFast_Downward_Wrapper::writeDomainPredicates()
{
	this->domain_file << "(:predicates \n";
	for (uint i = 0; i < this->domain_symbolic_predicates.size(); ++i)
	{
		if(not this->domain_symbolic_predicates[i].variables.size() == this->domain_symbolic_predicates[i].types.size())
		{
			std::cout << "ERROR writing the predicates, you do not have as many types as variables for the " << i << "variable.\n";
		}
		this->domain_file << "(" << this->domain_symbolic_predicates[i].name << " ";
		for (uint h = 0; h < this->domain_symbolic_predicates[i].variables.size(); ++h)
		{
			this->domain_file << "?" << this->domain_symbolic_predicates[i].variables[h] << " ";
			if(not strcmp(this->domain_symbolic_predicates[i].types[h].c_str(),"")==0)
				this->domain_file << "- " << this->domain_symbolic_predicates[i].types[h] << " ";
		}
		this->domain_file << ")\n";
	}
	this->domain_file << ")\n";	
}
void CFast_Downward_Wrapper::writeCost()
{
	if(this->use_cost)
		this->domain_file << "(:functions (total-cost) - number)\n";
}
void CFast_Downward_Wrapper::writeActions()
{
	if(this->domain_actions.size() == 0)
		std::cout << "WARNING: no actions have been defined.\n";

	for (uint i = 0; i < this->domain_actions.size(); ++i)
	{
		this->domain_file << "(:action " << this->domain_actions[i].action_name << "\n";

		this->domain_file << ":parameters (";

		if(this->domain_actions[i].parameters.params.size() != this->domain_actions[i].parameters.types.size())
		{
			std::cout << "WARNING: For the action " << i << " the vector of types is not properly defined. "
			<< "The parameters will be set without types.\n";
			this->domain_actions[i].parameters.types.resize(this->domain_actions[i].parameters.params.size());	
		}

		if(this->domain_actions[i].parameters.params.size() > 0)
		{
			for (uint h = 0; h < this->domain_actions[i].parameters.params.size(); ++h)
			{
				this->domain_file << "?" << this->domain_actions[i].parameters.params[h] << " ";  			

				if(this->domain_actions[i].parameters.types.size() > 0)
				if(not this->domain_actions[i].parameters.types[h].length() == 0)
					this->domain_file << "- " << this->domain_actions[i].parameters.types[h] << " "; 
			}
			
		}
		this->domain_file << ")\n";			

		this->domain_file << ":precondition (and";		
		this->domain_file << this->domain_actions[i].preconditions;
		this->domain_file << ")\n";			

		this->domain_file << ":effect (and";		
		this->domain_file << this->domain_actions[i].effects;
		this->domain_file << ")\n";
		
		this->domain_file << ")\n";			
	}	
}
void CFast_Downward_Wrapper::endDomain()
{
	this->domain_file << ")";
}
void CFast_Downward_Wrapper::callPlanner()
{
	std::string command;

	if(this->domain_folder.length() == 0)
	{
		std::cout << "WARNING: domain folder not set\n";
	}
	if(this->heuristic.length() == 0)
	{
		std::cout << "ERROR: heuristic not specified\n";
		return;
	}
	if(this->search.length() == 0)
	{
		std::cout << "ERROR: search method not specified\n";
		return;
	}

	command = this->fast_downward_folder + "/fast-downward.py ";
	
	if(this->domain_folder.length() > 0)
		command += this->domain_folder + "/" + domain_file_name + ".pddl ";
	else
		command += "./" + domain_file_name + ".pddl ";

	if(this->problem_folder.length() > 0)
		if(strcmp(this->problem_folder.c_str(),"./") == 0)
			command += this->problem_folder + problem_file_name + ".pddl ";
		else
			command += this->problem_folder + "/" + problem_file_name + ".pddl ";
	else
		command += "./" + problem_file_name + ".pddl ";

	command += "--heuristic \"" + this->heuristic + "\" ";
	command += "--search \"" + this->search + "\" ";

	std::cout << "COMMAND: \n" << command << std::endl;

	int res = std::system(command.c_str());
	if(res == 0)
	{
		std::cout << "Planner called succesfully\n";
	}
	else
	{
		std::cout << "ERROR: Problems calling the planner\n";	
	}
}
void CFast_Downward_Wrapper::setCurrentFolder(std::string current_folder)
{
	this->current_folder = current_folder;
}
void CFast_Downward_Wrapper::setFastDownwardFolder(std::string folder)
{
	this->fast_downward_folder = folder;
}
void CFast_Downward_Wrapper::setDomainFolder(std::string domain_folder)
{
	this->domain_folder = domain_folder;
}
void CFast_Downward_Wrapper::setDomainPDDLFile(std::string domain_file_name)
{
	this->domain_file_name = domain_file_name;
}

void CFast_Downward_Wrapper::setProblemPDDLFile(std::string problem_file_name)
{
	this->problem_file_name = problem_file_name;
}
void CFast_Downward_Wrapper::setObjects(Fast_Downward_Wrapper::Objects objects)
{
	this->objects = objects;
}
void CFast_Downward_Wrapper::setSymbolicPredicates(Fast_Downward_Wrapper::SymbolicPredicates sym_predicates)
{
	this->sym_predicates = sym_predicates;
}
void CFast_Downward_Wrapper::setProblemFolder(std::string problem_folder)
{
	this->problem_folder = problem_folder;
}
void CFast_Downward_Wrapper::setGoal(std::string goal)
{
	this->goal = goal;
}
void CFast_Downward_Wrapper::setHeuristic(std::string heuristic)
{
	this->heuristic = heuristic;
}
void CFast_Downward_Wrapper::setSearch(std::string search)
{
	this->search = search;
}
void CFast_Downward_Wrapper::setRequirements(std::vector<std::string> requirements)
{
	this->requirements = requirements;
}
void CFast_Downward_Wrapper::setDomainPredicates(Fast_Downward_Wrapper::DomainSymbolicPredicates domain_symbolic_predicates)
{
	this->domain_symbolic_predicates = domain_symbolic_predicates;	
}
void CFast_Downward_Wrapper::setDomainActions(Fast_Downward_Wrapper::DomainActions domain_actions)
{
	this->domain_actions = domain_actions;
}
void CFast_Downward_Wrapper::setTypes(std::vector<std::string> types)
{
	this->types = types;
}
void CFast_Downward_Wrapper::addTotalCost(uint init_cost)
{
	this->use_cost = true;
	this->init_cost = init_cost;
}
void CFast_Downward_Wrapper::writeProblemPDDLFile(bool over_write)
{
	std::cout << "Writing " << this->problem_file_name + ".pddl" << " file. in " << this->problem_folder + "/" + this->problem_file_name + ".pddl" << "\n";

	if(this->problem_file_name.length() == 0)
	{
		std::cout << "ERROR: Problem file name not specified\n";
		return;
	}
	if(this->problem_folder.length() == 0)
	{
		std::cout << "ERROR: Problem folder path not specified\n";
		return;
	}
	if(this->domain_file_name.length() == 0)
	{
		std::cout << "ERROR: Domain file name not specified\n";
		return;
	}
	if(this->objects.size() == 0)
	{
		std::cout << "ERROR: Objects not specified\n";
		return;
	}
	if(this->sym_predicates.size() == 0)
	{
		std::cout << "\n\nWARNING: Symbolic predicates not specified\n\n\n";
		
	}
	if(this->goal.length() == 0)
	{
		std::cout << "ERROR: Goal not specified\n";
		return;
	}

	// check if the file exist
	if(!over_write)
	{
		std::ifstream problem_file;
		problem_file.open((this->problem_folder + "/" + this->problem_file_name + ".pddl").c_str());
		if(problem_file.is_open())
			std::cout << "The file " << this->problem_file_name << " already exists. Exiting since no over writing behavior is set.\n";
	}
	else
		std::cout << "Over writing behavior on\n";

	this->problem_file.open((this->problem_folder + "/" + this->problem_file_name + ".pddl").c_str());
	if(this->problem_file.is_open())
	{
		this->startProblem();
		this->writeObjects();
		this->writePredicates();
		this->writeGoal();
		
		// write metric for the action cost
		if(this->use_cost)
			this->problem_file << "(:metric minimize (total-cost))\n";

		this->endProblem();
		this->problem_file.close();
		std::cout << "Problem file written.\n";
	}

	// check if the file exist
	std::ifstream problem_file;
	problem_file.open((this->problem_folder + "/" + this->problem_file_name + ".pddl").c_str());
	if(problem_file.is_open())
		std::cout << "The file " << this->problem_file_name << " was written correctly.\n";
	else
		std::cout << "ERROR: The file " << this->problem_file_name << " was NOT written correctly.\n";
	



}
void CFast_Downward_Wrapper::writeDomainPDDLFile(bool over_write)
{
	std::cout << "Writing " << this->domain_file_name + ".pddl" << " file. in " << this->domain_folder + "/" + this->domain_file_name + ".pddl" << "\n";

	if(this->domain_folder.length() == 0)
	{
		std::cout << "ERROR: Problem folder path not specified\n";
		return;
	}
	if(this->domain_file_name.length() == 0)
	{
		std::cout << "ERROR: Domain file name not specified\n";
		return;
	}
	if(this->domain_symbolic_predicates.size() == 0)
	{
		std::cout << "\n\nWARNING: Symbolic predicates not specified\n\n\n";
	}

	// check if the file exist
	if(!over_write)
	{
		std::ifstream domain_file;
		domain_file.open((this->domain_folder + "/" + this->domain_file_name + ".pddl").c_str());
		if(domain_file.is_open())
			std::cout << "The file " << this->domain_file_name << " already exists. Exiting since no over writing behavior is set.\n";
	}
	else
		std::cout << "Over writing behavior on\n";

	//writing the domain file
	this->domain_file.open((this->domain_folder + "/" + this->domain_file_name + ".pddl").c_str());
	if(this->domain_file.is_open())
	{
		this->startDomain();
		this->writeRequirements();
		this->writeTypes();
		this->writeDomainPredicates();
		this->writeCost();
		this->writeActions();
		this->endDomain();
		this->domain_file.close();
		std::cout << "Domain file written.\n";
	}

}
bool CFast_Downward_Wrapper::computePlan()
{
	//Checking that all the necessary things are set up
	std::cout << "\ncomputePlan(): Checking requirements ---";
	bool error_ = false; // to check if there was any error
	if(this->current_folder.length() == 0)
	{
		std::cout << "\nERROR:   Current folder not specified\n";
		error_ = true;
	}
	if(this->domain_folder.length() == 0)
	{
		std::cout << "\nERROR:   Domain folder not specified\n";
		error_ = true;
	}
	if(this->domain_file_name.length() == 0)
	{
		std::cout << "\nERROR:   Domain file name not specified\n";
		error_ = true;
	}
	if(this->problem_folder.length() == 0)
	{
		std::cout << "\nERROR:   Problem folder not specified\n";
		error_ = true;
	}
	if(this->problem_file_name.length() == 0)
	{
		std::cout << "\nERROR:   Problem file name not specified\n";
		error_ = true;
	}
	if(this->heuristic.length() == 0)
	{
		std::cout << "\nERROR:   Heuristic not specified\n";
		error_ = true;
	}
	if(this->search.length() == 0)
	{
		std::cout << "\nERROR:  Search method not specified\n";
		error_ = true;
	}
	if(error_)
		std::exit(-1);
	std::cout << " all requirements satisfied \n\n";

	// remove the sas_plan file
	std::string command = "rm sas_plan";
	std::system(command.c_str());

	this->callPlanner();

	Fast_Downward_Wrapper::Actions actions;
	unsigned int cost;

	std::string plan_file_name = "sas_plan";
	std::ifstream plan_file (plan_file_name.c_str());
  	if (plan_file.is_open())
  	{
  		while(1)
  		{
	  		std::string action;
	  		std::vector<std::string> objects_;

	  		// get the action
	 		plan_file >> action;

	 		//check if it is the last line:
			std::size_t last_line = action.find(";");
			if (last_line != std::string::npos)
			{
				std::string tmp; //string to discard the words
				plan_file >> tmp;
				plan_file >> tmp;
				plan_file >> cost;
				break;
			}

	 		// remove all the characters ( ) - 
	 		// reference: http://stackoverflow.com/questions/5891610/how-to-remove-characters-from-a-string
			char chars[] = "(";
			for (unsigned int i = 0; i < strlen(chars); ++i)
			{
			  // you need include <algorithm> to use general algorithms like std::remove()
			  action.erase (std::remove(action.begin(), action.end(), chars[i]), action.end());
			}

			// read action's objects of interest
	   		while(1)
	   		{	
	   			std::string object;
		   		// get objects of interest
				plan_file >> object;
				//check if in the object there is the character)   			
				std::size_t found = object.find(")");
		  		if (found != std::string::npos) //if there is the character ')' the action finished, so take off the ) character and start saving another action
		  		{	
		  			object.erase (std::remove(object.begin(), object.end(), ')'), object.end());
		  			objects_.push_back(object);
		  			break;
		  		}
		  		objects_.push_back(object);
	  		}
	  		//update plan
	  		Fast_Downward_Wrapper::Action action_;
	  		action_.action = action;
	  		action_.objects = objects_;
	  		actions.push_back(action_);

	  	}

    	plan_file.close();
  	}

  	Fast_Downward_Wrapper::Plan plan;
  	plan.actions = actions;
  	plan.cost = cost;
  	this->plan = plan;

  	// check if there is the sas_plan
	std::ifstream sas_plan_file;
	sas_plan_file.open("sas_plan");
	if(!sas_plan_file.is_open())
	{
		std::cout << "\n\nThere is NO a SOLUTION for such a problem!\n\n";
		return false;
	}

  	return true;
}
void CFast_Downward_Wrapper::printPlan()
{
	for (unsigned int i = 0; i < this->plan.actions.size(); ++i)
	{
		std::cout << "Action: " << this->plan.actions[i].action << " ";
		if(this->plan.actions[i].objects.size() > 0)
			std::cout << " with parameters: ";
		for (unsigned int h = 0; h < this->plan.actions[i].objects.size(); ++h)
		{
			std::cout << this->plan.actions[i].objects[h] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << "Plan with cost: " << this->plan.cost << "\n";
}

Fast_Downward_Wrapper::Plan CFast_Downward_Wrapper::getPlan()
{
	return this->plan;
}

std::string CFast_Downward_Wrapper::getActionGraspPrecondition(std::string obj)
{
	return "\n(not (exists (?x - obj)(on ?x "+ obj +")))\n(not (exists (?x - obj)(block_grasp ?x "+ obj +")))\n(not (ik_unfeasible_grasp " + obj + "))\n";
}
std::string CFast_Downward_Wrapper::getActionGraspEffect(std::string obj, uint cost)
{
	std::ostringstream convert; 
	convert << cost; 

	std::string effect_str;
	effect_str ="\n(removed " + obj + ")\n";
	effect_str +="(forall (?x - obj)\n(when (on " + obj + " ?x) (not (on " + obj + " ?x)))\n)\n";
	effect_str +="(forall (?x - obj)\n(when (block_grasp " + obj + " ?x) (not (block_grasp " + obj + " ?x)))\n)\n";
	effect_str +="(forall (?x - obj)\n(and\n(forall (?d - direction)\n(when (block_dir " + obj + " ?x ?d)(not (block_dir " + obj + " ?x ?d))))))\n";
	effect_str +="(forall (?x - obj)\n(and \n (forall (?d - direction)\n(when (ik_unfeasible_dir ?x ?d)(not (ik_unfeasible_dir ?x ?d)))\n)\n(when (ik_unfeasible_grasp ?x)(not (ik_unfeasible_grasp ?x)))\n)\n)\n";
	effect_str +="(increase (total-cost) "+ convert.str() + ")\n";	
	return effect_str;
}
std::string CFast_Downward_Wrapper::getGraspActionName(std::string obj)
{
	return "grasp-" + obj;
}
std::string CFast_Downward_Wrapper::getGraspActionName(uint obj)
{
	std::ostringstream convert; 
	convert << obj; 
	return "grasp-o" + convert.str();
}

std::string CFast_Downward_Wrapper::getActionPushPrecondition(std::string obj, std::string dir)
{
	return "\n(not (exists (?x - obj)(block_dir ?x " + obj + " " + dir + ")))\n(not (exists (?x - obj)(on ?x "+obj+")))\n(not (exists (?x - obj)(on "+obj+" ?x)))\n(not (ik_unfeasible_dir "+obj+" " + dir +"))";
}
std::string CFast_Downward_Wrapper::getActionPushEffect(std::string obj, std::string dir, uint cost)
{
	std::ostringstream convert; 
	convert << cost; 

	std::string effect_str;
	effect_str ="\n(forall (?x - obj)\n(and\n(forall (?d - direction)\n(and\n(when (block_dir "+obj+" ?x ?d) (not (block_dir "+obj+" ?x ?d)))\n";
	effect_str += "(when (block_dir ?x "+obj+ " ?d) (not (block_dir ?x "+obj+ " ?d)))\n";
	effect_str += "(when (ik_unfeasible_dir ?x ?d)(not (ik_unfeasible_dir ?x ?d)))\n)\n)\n";
	effect_str +="(when (block_grasp "+obj+" ?x) (not (block_grasp "+obj+" ?x)))\n(when (block_grasp ?x "+obj+") (not (block_grasp ?x "+obj+")))\n";
	//effect_str +="(when (ik_unfeasible_grasp "+obj+") (not (ik_unfeasible_grasp "+obj+")))\n";
	//effect_str +="(when (ik_unfeasible_grasp "+obj+") (not (ik_unfeasible_grasp "+obj+")))\n";
	effect_str +="(when (ik_unfeasible_grasp ?x)(not (ik_unfeasible_grasp ?x)))\n)\n)\n";
	effect_str +="(increase (total-cost) "+ convert.str() + ")\n";	
	
	return effect_str;				 
}
std::string CFast_Downward_Wrapper::getPushActionName(std::string obj, std::string dir)
{
	return "push-" + obj + "-" + dir;
}
std::string CFast_Downward_Wrapper::getPushActionName(uint obj, uint dir)
{
	std::ostringstream convert; 
	convert << obj; 	
	std::string action_name = "push-o" + convert.str();
	convert.str(""); // reset
	convert << dir; 	
	action_name += "-dir" + convert.str();
	return action_name;
}